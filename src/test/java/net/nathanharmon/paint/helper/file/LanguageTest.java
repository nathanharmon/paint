/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.helper.file;

import net.nathanharmon.paint.Main;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LanguageTest {

    @Test
    public void getText() {
        if(Main.isNotLogging())
            Main.setupLogging();

        Language.load("en");

        assertEquals("File", Language.getText("file"));
    }
}