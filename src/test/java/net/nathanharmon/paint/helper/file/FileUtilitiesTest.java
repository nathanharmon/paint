/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.helper.file;

import net.nathanharmon.paint.Main;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Level;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileUtilitiesTest {

    private static MessageDigest digest;

    @BeforeAll
    public static void setupDigest() {
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Error loading SHA-256 algorithm.");
        }
    }

    @Test
    public void getResourcesFile() {
        if(Main.isNotLogging())
            Main.setupLogging();

        File file = FileUtilities.getResourcesFile("lang/en.yaml");

        if(file != null && digest != null) {
            InputStream stream;

            try {
                stream = new FileInputStream(file);

                assertEquals("[-64, 80, -116, -66, -30, 9, 78, -79, -108, 75, 99, -4, 87, 42, -10, 64, 39, 79, -8, 14, 107, 116, 45, -92, 38, 112, -11, 115, 53, 53, 63, -78]",
                        Arrays.toString(digest.digest(stream.readAllBytes())));
            } catch (FileNotFoundException e) {
                Main.log(Level.SEVERE, "Input stream not loaded for resources file unit test.");
            } catch (IOException e) {
                Main.log(Level.SEVERE, "Error reading file for resources file unit test.");
            }
        }
    }

    @Test
    public void getResourcesStream() {
        if(Main.isNotLogging())
            Main.setupLogging();

        InputStream stream = FileUtilities.getResourcesStream("lang/en.yaml");

        if(stream != null && digest != null)
            assertEquals(733957003, stream.hashCode());
    }
}