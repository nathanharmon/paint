/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint;

import net.nathanharmon.paint.helper.file.FileUtilitiesTest;
import net.nathanharmon.paint.helper.file.LanguageTest;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.util.logging.Level;

/**
 * JUnit Main class for tests.
 */
public class TestMain {

    // Adapted from: https://www.tutorialspoint.com/junit/junit_basic_usage.htm

    public static void main(String[] args) {
        if(Main.isNotLogging())
            Main.setupLogging();

        /*
         * Run Tests
         */
        Result result = JUnitCore.runClasses(
                LanguageTest.class,
                FileUtilitiesTest.class);

        for(Failure failure : result.getFailures())
            Main.log(Level.WARNING, failure.toString());

        Main.log(Level.INFO, "Tests Completed Successfully: %b", result.wasSuccessful());
    }
}
