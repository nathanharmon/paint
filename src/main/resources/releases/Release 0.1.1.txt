Pain(t) | RELEASE version 0.1.1 | 09/16/2021 | Nathan Harmon

Features:
- (NEW) Save
- (NEW) Save As
- (NEW) View Release Notes
- (NEW) Add first canvas tool: line
- (NEW) Menu items now have keyboard shortcuts
- (NEW) Canvas can now be drawn on
- (NEW) Thickness of tools can be modified
- (NEW) Underlying codebase framework written for menu items
- (NEW) Underlying codebase framework written for canvas and tools
- (NEW) Release notes can now be viewed in a window under Help -> Release Notes
- About Pain(t) window created
- Images can now be opened onto the drawing area
- Menu bar created
- Logging is centrally located

Known Issues:
- HEIC, WebP, SVG, FF file formats do not work
- "Exit" does not check for modifications before closing
- Color chooser does not modify line
- Lines don't appear in save

Previous Issues Resolved:

Future Items:
- Check for updates menu item
- Strings/language file