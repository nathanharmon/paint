Pain(t) | RELEASE version 0.1.2 | 09/24/2021 | Nathan Harmon

Features:
- (NEW) Zooming the canvas
- (NEW) Rectangle and Filled Rectangle Tools
- (NEW) Square and Filled Square Tools
- (NEW) Ellipse and Filled Ellipse Tools
- (NEW) Circle and Filled Circle Tools
- (NEW) Freehand draw tool
- (NEW) Text Tool
- (NEW) Prompt user to save before closing
- (NEW) Tabs
- (NEW) Optimizations with file extensions
- (NEW) Open a blank canvas in new tab with File -> New
- (NEW) Rename directory from xyz.nathanharmon to net.nathanharmon
- Save
- Save As
- View Release Notes
- Add first canvas tool: line
- Menu items now have keyboard shortcuts
- Canvas can now be drawn on
- Thickness of tools can be modified
- Underlying codebase framework written for menu items
- Underlying codebase framework written for canvas and tools
- Release notes can now be viewed in a window under Help -> Release Notes
- About Pain(t) window created
- Images can now be opened onto the drawing area
- Menu bar created
- Logging is centrally located

Known Issues:
- HEIC, WebP, SVG, FF file formats do not work
- Lines don't appear in save

Previous Issues Resolved:
- Color chooser does not modify line
- "Exit" does not check for modifications before closing

Future Items:
- Check for updates menu item
- Strings/language file