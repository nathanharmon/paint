Pain(t) | RELEASE version 1.0.0 | 12/25/2021 | Nathan Harmon

Features:
- (NEW) Stable release with Jar build and CI/CD.
- Create Jar File
- Export Javadoc
- Modify resources accessor method to allow javadoc use.
- Implement whatgoeshere.txt feature describing the contents of each folder.
- Images and lang files are now stored within their own folder in the resources folder
- Change cursor icon when dropper tool is selected
- Logging now logs to .cache/paint/ folder
- Add copy and move tool
- Display label for the currently selected tool
- Add autosave feature
- Setup config file and autosave cache location based on operating system
- Add JUnit unit tests
- Clean up tool pane view
- Add logging to file
- Add icons for tools
- Fix bug with cut and draw not removing the cut portion
- Check for updates
- Rounded Squares and Rectangles
- N sided polygon tool
- Default image display on open.
- The Paint window now has an icon
- Undo and redo
- Cut and move tool
- Drawing can now be viewed live
- Internal code optimization and organization
- Prompt  the user for the height and width of a new canvas
- Add language/locales support
- Zooming the canvas
- Rectangle and Filled Rectangle Tools
- Square and Filled Square Tools
- Ellipse and Filled Ellipse Tools
- Circle and Filled Circle Tools
- Freehand draw tool
- Text Tool
- Prompt user to save before closing
- Tabs
- Optimizations with file extensions
- Open a blank canvas in new tab with File -> New
- Rename directory from xyz.nathanharmon to net.nathanharmon
- Save
- Save As
- View Release Notes
- Add first canvas tool: line
- Menu items now have keyboard shortcuts
- Canvas can now be drawn on
- Thickness of tools can be modified
- Underlying codebase framework written for menu items
- Underlying codebase framework written for canvas and tools
- Release notes can now be viewed in a window under Help -> Release Notes
- About Pain(t) window created
- Images can now be opened onto the drawing area
- Menu bar created
- Logging is centrally located

Known Issues:
- HEIC, WebP, SVG, FF file formats do not work

Previous Issues Resolved:

Future Items: