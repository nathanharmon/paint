/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.helper;

import javafx.application.Platform;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.drawing.CanvasTab;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.FileUtilities;
import net.nathanharmon.paint.windows.MainWindow;

import java.io.File;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Level;

/**
 * Start thread to save each tab's changes at a regular interval independent of the regular (manual) save function.
 */
public class Autosave extends TimerTask {

    private final TabManager tabs;
    private final String path;
    private final MainWindow window;

    public Autosave(TabManager tabs, String path, MainWindow window) {
        this.tabs = tabs;
        this.path = path;
        this.window = window;
    }

    @Override
    public void run() {
        Platform.runLater(() -> {
            for (CanvasTab tab : tabs.getAllTabs()) {
                // only autosave if the tab is not already saved
                if(!tab.getActions().isSaved()) {
                    // create path to this file to save
                    String saveFile = path + tab.getText() + "-" + new Date() + ".png";
                    File file = new File(saveFile);

                    FileUtilities.writeImageToFile(tab.getActions().getImage(), file);
                    Main.log(Level.INFO, "Autosave: saving file to %s.", saveFile);

                    try {
                        window.animateSaveTab();
                    } catch (InterruptedException e) {
                        Main.log(Level.WARNING, "The save animation was interrupted.");
                    }
                }
            }
        });
    }
}
