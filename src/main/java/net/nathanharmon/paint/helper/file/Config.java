/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.helper.file;

import net.nathanharmon.paint.Main;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

public class Config {

    private static final ArrayList<Config> instances = new ArrayList<>();

    private final File file;
    private final Yaml yaml;
    private HashMap<String, String> data;

    public Config(File file) {
        this.file = file;
        yaml = new Yaml();

        try {
            data = yaml.load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            Main.log(Level.WARNING, "Failed to load config file: %s.", file.getName());
            data = new HashMap<>();
        }

        instances.add(this);
    }

    /**
     * Get the data at the "location" index.
     * @param location The config parameter to get.
     * @return The value at location. Empty string if nothing is at the location.
     */
    public String getData(String location) {
        return data.getOrDefault(location, "");
    }

    /**
     * Get the data at the "location" index or the default if null.
     * @param location The config parameter to get.
     * @param def The default string to return if there is no value at that location.
     * @return The value at location. "def" if nothing is at the location.
     */
    public String getDataOrDefault(String location, String def) {
        return getData(location).equals("") ? def : getData(location);
    }

    /**
     * Write data to the file at location with data, value.
     * @param location Location to place the data in the config file.
     * @param value The data to put in the config file.
     */
    public void writeData(String location, String value) {
        data.put(location, value);
    }

    /**
     * Save the file to disk.
     */
    public void closeAndSave() {
        try {
            yaml.dump(data, new FileWriter(file));
        } catch (IOException e) {
            Main.log(Level.SEVERE, "Failed to save config file %s.", file.getName());
        }
    }

    /**
     * Save all config files that are open to disk.
     */
    public static void closeAll() {
        for(Config c : instances) {
            c.closeAndSave();
        }
    }
}
