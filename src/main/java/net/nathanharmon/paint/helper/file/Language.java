/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.helper.file;

import org.yaml.snakeyaml.Yaml;

import java.util.Map;

/**
 * Get text that is displayed to the user from a specific file.
 */
public class Language {

    private static Map<String, Object> values;

    /**
     * Load language file. Should be run once at the start of the program.
     * @param lang Language to choose.
     * "en": English
     */
    public static void load(String lang) {
        Yaml yaml = new Yaml();

        values = yaml.load(FileUtilities.getResourcesStream("lang/" + lang + ".yaml"));
    }

    /**
     * Find what text to display in the current language at the given position.
     * @param location Path to the string to display.
     * @return String in proper language.
     */
    public static String getText(String location) {
        return (String) values.get(location);
    }
}
