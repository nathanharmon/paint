/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

import java.awt.image.BufferedImage;

/**
 * Select an area of the canvas and move it to another place.
 */
public class CopyAndMove extends CanvasTool {

    private Image image;

    /**
     * Create new Canvas Tool
     *
     * @param tabs Tab manager object (to access the tool selection).
     */
    public CopyAndMove(TabManager tabs) {
        super(tabs, Language.getText("copyandmove"), "copyandmove");

        // override button press action event from CanvasTool
        setOnAction(event -> {
            tabs.setSelectedTool(this);

            // reset cursor to standard cursor
            tabs.setCursor(Cursor.DEFAULT);

            // when button is pressed, clear the image variable to allow for new selection to be made
            image = null;
        });
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        if(image == null) { // an image has not yet been selected. Select a new image.
            // get a picture of the entire canvas
            Image fullImage = getActions().getImage();

            // convert Image to buffered image (because a buffered image has a .getSubImage() method)
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(fullImage, null);

            // calculate the x, y, width, and height of the rectangle
            double[] rect = calculateRect(sx, sy, ex, ey);

            // set image to the cropped image
            BufferedImage subImage = new BufferedImage((int) rect[2], (int) rect[3], BufferedImage.OPAQUE);
            subImage.createGraphics().drawImage(bufferedImage.getSubimage((int) rect[0], (int) rect[1], (int) rect[2], (int) rect[3]), 0, 0, null);

            image = SwingFXUtils.toFXImage(subImage, null);
        } else { // an image exists. Paste this new image.
            // get min of sx, ex and sy, ey to correspond with the top left
            getActions().drawImage(image, sx, sy);
        }
    }

    @Override
    public void drag(double x, double y) {}
}
