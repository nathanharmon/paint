/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Draw a Right Triangle without filling.
 */
public class RightTriangle extends CanvasTool {

    /**
     * Create new Canvas Tool
     *
     * @param tabs Tab manager object (to access the tool selection).
     */
    public RightTriangle(TabManager tabs) {
        super(tabs, Language.getText("righttriangle"), "righttriangle");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setStroke(getTabs().getPaint());

        getGraphics().strokePolygon(new double[] {sx, sx, ex}, new double[] {sy, ey, ey}, 3);
    }

    @Override
    public void drag(double x, double y) {}
}
