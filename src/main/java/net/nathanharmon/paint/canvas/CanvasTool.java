/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Pair;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.drawing.CanvasActions;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.FileUtilities;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.Level;

public abstract class CanvasTool extends Button {

    private final String name;
    private final TabManager tabs;

    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object (to access the tool selection).
     * @param name Name of the canvas tool.
     */
    public CanvasTool(TabManager tabs, String name, @NotNull String iconPath) {
        this.tabs = tabs;
        this.name = name;

        // set hover text when user hovers over button
        setAccessibleText(name);
        setTooltip(new Tooltip(name));

        // make button have icon
        ImageView icon = new ImageView(new Image(Objects.requireNonNull( // if there is a null pointer exception to this line, it is probably because "iconPath" does not reference an actual file
                FileUtilities.getResourcesStream("images/icon-" + iconPath + ".png"))));

        icon.setPreserveRatio(true);
        icon.setFitHeight(20);
        setGraphic(icon);

        // when button is pressed, select this tool
        // overridden in certain tools
        setOnAction(event -> {
            tabs.setSelectedTool(this);

            // reset cursor to standard cursor
            tabs.setCursor(Cursor.DEFAULT);

            Main.log(Level.INFO, "Switched tool to %s.", name);
        });
    }

    // for geometric shapes
    public abstract void draw(double sx, double sy, double ex, double ey);

    // for freehand shapes
    public abstract void drag(double x, double y);

    /**
     * Calculate the proper x, y, width, and height values given the coordinate pairs of its corners.
     * 
     * @param sx The x coordinate of the mouse when it is pressed.
     * @param sy The y coordinate of the mouse when it is pressed.
     * @param ex The x coordinate of the mouse when it is released.
     * @param ey The y coordinate of the mouse when it is released.
     * @return A double array consisting of 0: x; 1: y; 2: w; 3: h; Can be used such as .strokeRect(rect[0], rect[1], rect[2], rect[3]);
     * @see GraphicsContext#strokeRect(double, double, double, double) 
     */
    protected static double[] calculateRect(double sx, double sy, double ex, double ey) {
        /*
        The goal of x and y is to always be the top left point on the rectangle.
         */

        // get the lower (further left) x mouse value (whether it is where the mouse started or ended)
        double x = Math.min(sx, ex);
        // get the lower (further up) y mouse value (whether it is where the mouse started or ended)
        double y = Math.min(sy, ey);

        /*
        get the width of the rectangle, if ex is greater, then simply subtracting sx from ex gives the width
        if sx is greater, subtracting ex from sx will return the width (but negative)
        simplifying this will show that the width is always the absolute value of ex-sx
        double w = sx < ex ? ex-sx : -(sx-ex);
        
        The same process can be applied for height. Simplifying will show that the absolute value of ey-sy is always the height.
        double h = sy < ey ? -(ey-sy) : sy-ey;
        */
        double w = Math.abs(ex-sx);
        double h = Math.abs(ey-sy);
        
        return new double[] {x, y, w, h};
    }

    /**
     * Calculate each vertex of a regular polygon.
     * @param centerX The x coordinate of the center of the polygon.
     * @param centerY The y coordinate of the center of the polygon.
     * @param initialX An x coordinate of the polygon's vertices. Corresponds with "initialY".
     * @param initialY A y coordinate of the polygon's vertices. Corresponds with "initialX".
     * @param sides The number of sides in the polygon. Must be greater than 2 (otherwise it would not be a polygon).
     * @return A list of (x, y) coordinates
     */
    protected static Pair<double[], double[]> getPolygonPoints(double centerX, double centerY, double initialX, double initialY, int sides) {

        // Create an array of x and y points (with size of the amount of sides)
        double[] xPoints = new double[sides];
        double[] yPoints = new double[sides];

        // 2 * PI = 360 degrees. Math.sin and Math.cos use Radians.
        // Divide by the amount of sides so "angle" is the angle between two vertices from the center.
        double angle = 2 * Math.PI / sides;

        // find the distance from the center point (centerX, centerY) to one of the vertices (initialX, initialY); acts as the hypotenuse in sin and cos
        double radius = Math.sqrt(Math.pow(initialX - centerX, 2) + Math.pow(initialY - centerY, 2));

        for (int i = 0; i < sides; i++) {
            // loop the angle around the polygon, so all points are covered.
            double theta = i * angle;

            /*
            Using SOH-CAH-TOA (dang geometry class coming in useful again)
            Note: radius is the hypotenuse.

            cos(theta) = x / radius
            sin(theta) = y / radius

            Solve for x, y.

            x = radius * cos(theta)
            y = radius * sin(theta)

            The above provides the *change* in x or y. Add centerX and centerY to get the actual coordinates.
            */
            xPoints[i] = centerX + (radius * Math.cos(theta));
            yPoints[i] = centerY + (radius * Math.sin(theta));
        }

        return new Pair<>(xPoints, yPoints);
    }

    public String getName() {
        return name;
    }

    /**
     * Get the canvas actions object of the currently selected tab.
     * @return CanvasActions object.
     */
    public CanvasActions getActions() {
        return tabs.getCurrentlySelectedTab().getActions();
    }

    /**
     * Get the canvas object of the currently selected tab.
     * @return Canvas object.
     */
    public Canvas getCanvas() {
        return tabs.getCurrentlySelectedTab().getCanvas();
    }

    /**
     * Get the graphics object of the currently selected tab.
     * @return GraphicsContext object.
     */
    public GraphicsContext getGraphics() {
        return getCanvas().getGraphicsContext2D();
    }

    /**
     * Get the tab manager object.
     * @return TabManager.
     */
    public TabManager getTabs() {
        return tabs;
    }
}
