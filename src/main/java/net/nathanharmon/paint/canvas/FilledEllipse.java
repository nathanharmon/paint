/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Draw an ellipse with filling.
 */
public class FilledEllipse extends CanvasTool {


    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public FilledEllipse(TabManager tabs) {
        super(tabs, Language.getText("filled") + Language.getText("ellipse"), "ellipse-filled");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setFill(getTabs().getPaint());

        double[] rect = calculateRect(sx, sy, ex, ey);

        getGraphics().fillOval(rect[0], rect[1], rect[2], rect[3]);
    }

    @Override
    public void drag(double x, double y) {}
}
