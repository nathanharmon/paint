/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Create a straight line on the canvas.
 */
public class Line extends CanvasTool {
    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public Line(TabManager tabs) {
        super(tabs, Language.getText("line"), "line");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setStroke(getTabs().getPaint());
        getGraphics().strokeLine(sx, sy, ex, ey);
    }

    @Override
    public void drag(double x, double y) {}
}
