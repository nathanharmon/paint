/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import javafx.scene.control.ColorPicker;
import javafx.scene.layout.GridPane;
import net.nathanharmon.paint.drawing.TabManager;

/**
 * Manage Canvas tools.
 */
public class CanvasToolManager {

    private final GridPane toolPane;
    private final TabManager tabs;
    private final ColorPicker picker;

    public CanvasToolManager(GridPane toolPane, TabManager tabs, ColorPicker picker) {
        this.toolPane = toolPane;
        this.tabs = tabs;
        this.picker = picker;
    }

    /**
     * Register the many canvas tools.
     */
    public void register() {
        /*
         * Add Tool Menu Items
         */

        toolPane.add(tabs.getSelectedTool(), 0, 0); // Line tool, this was already constructed by default in TabManager's constructor. Other tools will be constructed here after this.
        toolPane.add(new Draw(tabs), 1, 0);
        toolPane.add(new Text(tabs), 2, 0);
        toolPane.add(new Eraser(tabs), 3, 0);

        toolPane.add(new Rectangle(tabs), 0, 1);
        toolPane.add(new FilledRectangle(tabs), 1, 1);
        toolPane.add(new RoundRectangle(tabs), 2, 1);
        toolPane.add(new FilledRoundRectangle(tabs), 3, 1);

        toolPane.add(new Square(tabs), 0, 2);
        toolPane.add(new FilledSquare(tabs), 1, 2);
        toolPane.add(new RoundSquare(tabs), 2, 2);
        toolPane.add(new FilledRoundSquare(tabs), 3, 2);

        toolPane.add(new Ellipse(tabs), 0, 3);
        toolPane.add(new FilledEllipse(tabs), 1, 3);
        toolPane.add(new Circle(tabs), 2, 3);
        toolPane.add(new FilledCircle(tabs), 3, 3);

        toolPane.add(new ScaleneTriangle(tabs), 0, 4);
        toolPane.add(new FilledScaleneTriangle(tabs), 1, 4);
        toolPane.add(new RightTriangle(tabs), 2, 4);
        toolPane.add(new FilledRightTriangle(tabs), 3, 4);

        toolPane.add(new Polygon(tabs), 0, 5);
        toolPane.add(new FilledPolygon(tabs), 1, 5);
        toolPane.add(new CutAndMove(tabs), 2, 5);
        toolPane.add(new CopyAndMove(tabs), 3, 5);

        toolPane.add(new DropperTool(picker, tabs), 0, 6);
    }
}
