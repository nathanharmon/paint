/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.canvas;

import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.Language;

/**
 * Draw a circle with filling.
 */
public class FilledCircle extends CanvasTool {


    /**
     * Create new Canvas Tool
     * @param tabs Tab manager object.
     */
    public FilledCircle(TabManager tabs) {
        super(tabs, Language.getText("filled") + Language.getText("circle"), "circle-filled");
    }

    @Override
    public void draw(double sx, double sy, double ex, double ey) {
        getGraphics().setLineWidth(getTabs().getLineWidth());
        getGraphics().setFill(getTabs().getPaint());

        double[] rect = calculateRect(sx, sy, ex, ey);

        getGraphics().fillOval(rect[0], rect[1], rect[2], rect[2]);
    }

    @Override
    public void drag(double x, double y) {}
}
