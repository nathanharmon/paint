/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.windows;

import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

/**
 * The  controller class for the "new window", the window that prompts the user for the size of a canvas.
 */
public class NewWindow extends WindowController {

    @FXML
    private Slider xSlider;

    @FXML
    private TextField xText;

    @FXML
    private Slider ySlider;

    @FXML
    private TextField yText;

    @Override
    public void onOpen() {
        xSlider.valueProperty().addListener((observable, oldValue, newValue) -> xText.setText(String.valueOf(xSlider.valueProperty().intValue())));

        ySlider.valueProperty().addListener((observable, oldValue, newValue) -> yText.setText(String.valueOf(ySlider.valueProperty().intValue())));

        xText.setOnKeyPressed(event -> {
            try {
                double value = Double.parseDouble(xText.getText());
                xSlider.setValue(value);
            } catch (NumberFormatException e) {
                // do nothing, the user just entered a non number
            }
        });

        yText.setOnKeyPressed(event -> {
            try {
                double value = Double.parseDouble(yText.getText());
                ySlider.setValue(value);
            } catch (NumberFormatException e) {
                // do nothing, the user just entered a non number
            }
        });
    }

    public double getX() {
        return xSlider.getValue();
    }

    public  double getY() {
        return ySlider.getValue();
    }
}
