/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.drawing;

import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.canvas.CanvasTool;
import net.nathanharmon.paint.canvas.Line;
import net.nathanharmon.paint.helper.file.Config;
import net.nathanharmon.paint.helper.file.FileUtilities;
import net.nathanharmon.paint.helper.file.Language;

import java.io.File;
import java.util.logging.Level;

public class TabManager {

    private CanvasTool selectedTool;
    private final Label selectedLabel;

    private double lineWidth;
    private Paint paint;
    private double zoom;

    private long autosaveInterval;

    private final TabPane pane;
    private CanvasTab currentlySelectedTab;

    private final Stage stage;

    public TabManager(TabPane pane, Stage stage, Label selectedLabel) {
        this.pane = pane;
        this.stage = stage;
        this.selectedLabel = selectedLabel;

        // only the currently open tab will be closable
        pane.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
        // make tabs draggable
        pane.setTabDragPolicy(TabPane.TabDragPolicy.REORDER);

        lineWidth = 1;
        selectedTool = new Line(this);
        paint = Color.BLACK;
        zoom = 50; // initial zoom percent is 50%

        Config config = new Config(new File(FileUtilities.getCacheFolder() + "data.yaml"));

        autosaveInterval = Long.parseLong(config.getDataOrDefault("autosaveInterval", "120000")); // default is 2 minutes (120000 in milliseconds).
    }

    /**
     * Create a new tab and add it to the tabs list.
     * @param title Tab title. If null, will default to Untitled.
     * @param switchTo Should the GUI switch focus to this tab upon open?
     * @return Newly constructed tab object.
     */
    public CanvasTab createTab(String title, boolean switchTo) { // TODO: fix switchTo not working
        // Create new tab with default title of untitled
        CanvasTab tab = new CanvasTab(this, title == null ? Language.getText("untitled") : title);

        Main.log(Level.INFO, "New tab created with title %s.", title);

        // this needs to be here and not in the constructor of CanvasTab because the object isn't fully available in the constructor
        tab.setOnCloseRequest(event -> {
            if(!tab.getActions().isSaved()) {
                FileUtilities.ClosingPromptResponse response = FileUtilities.promptToSaveOnClose(tab.getText());

                switch (response) {
                    case SAVE -> {
                        File file;

                        if(tab.getActions().isSaved())
                            file = tab.getActions().getSaveFile();
                        else
                            file = FileUtilities.saveImage(Language.getText("save"), tab.getText());

                        assert file != null; // if a tab is saved, then the save file cannot be null
                        FileUtilities.writeImageToFile(tab.getActions().getImage(), file);
                    }
                    case EXIT -> {
                        // continue doing what the program was doing
                    }
                    case CANCEL -> {
                        //TODO cancel event
                        event.consume();
                    }
                }

            }

            // if closing this tab will remove all tabs, create new tab
            if(pane.getTabs().size() == 1)
                createTab(null, true);
        });

        tab.setOnSelectionChanged(event -> {
            int i = 0;
            boolean found = false;

            while(!found && getAllTabs().length > i) {
                if(getAllTabs()[i].isSelected()) {
                    currentlySelectedTab = getAllTabs()[i];
                    found = true;
                }
                i++;
            }
        });

        // add this tab to the list of tabs
        pane.getTabs().add(tab);

        return tab;
    }

    /**
     * Returns the currently selected tab.
     */
    public CanvasTab getCurrentlySelectedTab() {
        return currentlySelectedTab;
    }

    /**
     * Set a tab to be the currently selected tab.
     * Should already be created and added in TabManager.
     * @see #createTab(String, boolean)
     * @param currentlySelectedTab Tab to set as the currently selected tab.
     */
    public void setCurrentlySelectedTab(CanvasTab currentlySelectedTab) {
        this.currentlySelectedTab = currentlySelectedTab;
    }

    public CanvasTab[] getAllTabs() {
        // warning can be ignored, there will only be CanvasTabs in the list
        //noinspection SuspiciousToArrayCall
        return pane.getTabs().toArray(CanvasTab[]::new);
    }

    /**
     * Exit the program gracefully. Prompt the user to save each open tab (if unsaved).
     */
    public void safeExit() {
        boolean cancelled = false;

        // Close all open config files and save to disk.
        Config.closeAll();

        for(CanvasTab tab : getAllTabs()) {
            if(cancelled) // if the user pressed "cancel" on the previous menu item, break out of loop
                break;

            if(!tab.getActions().isSaved()) {
                FileUtilities.ClosingPromptResponse response = FileUtilities.promptToSaveOnClose(tab.getText());

                switch (response) {
                    case SAVE -> {
                        File file;

                        if(tab.getActions().isSaved())
                            file = tab.getActions().getSaveFile();
                        else
                            file = FileUtilities.saveImage(Language.getText("save"), tab.getText());

                        assert file != null; // if a tab is saved, then the save file cannot be null
                        FileUtilities.writeImageToFile(tab.getActions().getImage(), file);

                        // in case the exiting of the program is cancelled, we still need to mark this tab as saved
                        tab.getActions().setSaved(true);
                        tab.getActions().setSaveFile(file);
                    }
                    case EXIT -> {} // Don't do anything. Don't save this tab.
                    case CANCEL -> cancelled = true;
                }
            }
        }

        if(!cancelled) // if the user did not click "cancel", then exit.
            System.exit(0);
    }

    // Tool Properties

    /**
     * Get the current line width.
     */
    public double getLineWidth() {
        return lineWidth;
    }

    /**
     * Update the line width.
     * @param lineWidth Width of line.
     */
    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    /**
     * Get currently selected tool.
     */
    public CanvasTool getSelectedTool() {
        return selectedTool;
    }

    /**
     * Update the current tool.
     */
    public void setSelectedTool(CanvasTool selectedTool) {
        this.selectedTool = selectedTool;
        selectedLabel.setText(selectedTool.getName());
    }

    /**
     * Get the current brush paint.
     * @return Paint object of the current paint.
     */
    public Paint getPaint() {
        return paint;
    }

    /**
     * Set the current brush paint.
     * @param paint Paint to set for the brushes.
     */
    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    /**
     * Set the zoom value.
     * @param zoom Percent zoom (between 15% and 200%)
     */
    public void setZoom(double zoom) {
        // ensure the zoom is within bounds (arbitrarily set, but it makes it look better)
        if(zoom > 200)
            zoom = 200;
        else if(zoom < 15)
            zoom = 15;

        this.zoom = zoom;

        for(CanvasTab tab : getAllTabs()) {
            tab.getCanvas().setScaleX(zoom/100); // divide by 100 to turn 100% into 1.0 and 50% into 0.5
            tab.getCanvas().setScaleY(zoom/100);
        }
    }

    /**
     * Return the zoom value.
     * @return Percent zoom (between 15% and 200%)
     */
    public double getZoom() {
        return zoom;
    }

    public long getAutosaveInterval() {
        return autosaveInterval;
    }

    public void setAutosaveInterval(long autosaveInterval) {
        this.autosaveInterval = autosaveInterval;
    }

    /**
     * Forwarding function to set the cursor while in the "Main" window.
     * @param cursor Cursor object to set the cursor to.
     */
    public void setCursor(Cursor cursor) {
        stage.getScene().setCursor(cursor);
    }
}
