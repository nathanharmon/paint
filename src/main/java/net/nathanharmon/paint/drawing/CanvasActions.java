/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.drawing;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.nathanharmon.paint.Main;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.InputStream;
import java.util.logging.Level;

/**
 * Perform various actions on the canvas.
 */
public class CanvasActions {

    private final Canvas canvas;

    @Nullable
    private File saveFile;

    private boolean saved;

    public CanvasActions(Canvas canvas) {
        this.canvas = canvas;
        saveFile = null;
        saved = false;
    }

    /**
     * Draw an image onto the canvas. Sets the initial canvas values.
     * @param stream Stream of the image to put on the canvas.
     */
    public void drawInitialImage(@NotNull InputStream stream) {
        Image image = new Image(stream);

        canvas.setWidth(image.getWidth());
        canvas.setHeight(image.getHeight());

        canvas.setScaleX(0.5);
        canvas.setScaleY(0.5);

        Main.log(Level.INFO, "Drawing initial image from file.");

        drawImage(image, 0, 0);
    }

    /**
     * Draw an image onto the canvas.
     * @param image Image file to be drawn to the canvas.
     */
    public void drawImage(@NotNull Image image, double x, double y) {
        canvas.getGraphicsContext2D().drawImage(image, x, y);
    }

    /**
     * Create blank canvas. Note: there should not be any other things written to the canvas before this.
     * @param width Width of the canvas.
     * @param height Height of the canvas.
     */
    public void createCanvas(double width, double height) {
        canvas.setWidth(width);
        canvas.setHeight(height);

        // draw initial canvas
        canvas.getGraphicsContext2D().setFill(Color.WHITE);
        canvas.getGraphicsContext2D().fillRect(0, 0, width, height);
    }

    /**
     * Return an image object of the current canvas.
     * @return Image
     */
    public Image getImage() {
        // get current zoom factors
        double xScale = canvas.getScaleX();
        double yScale = canvas.getScaleY();

        // set current zoom to 100%
        canvas.setScaleX(1);
        canvas.setScaleY(1);

        // get full resolution image
        Image image = canvas.snapshot(null, null);

        // reset zoom scale to original value
        canvas.setScaleX(xScale);
        canvas.setScaleY(yScale);

        return image;
    }

    /**
     * After a file has been opened, this will be the file location.
     * @return File object of where to save.
     */
    public @Nullable File getSaveFile() {
        return saveFile;
    }

    /**
     * When a file is opened, store the file object for later use when saving (but not save as)
     * @param saveFile File to store.
     */
    public void setSaveFile(@Nullable File saveFile) {
        this.saveFile = saveFile;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }
}
