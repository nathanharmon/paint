/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus;

import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.windows.WindowController;

import java.io.IOException;
import java.util.logging.Level;

/**
 * Utility Class
 * Purpose: for when a menu item's only purpose is to open a specific window (*.fxml) file.
 */
public class MenuItemToWindow extends MenuItem {

    private Stage stage;
    private WindowController menu;

    /**
     * Simply set the information in a menu item to open a specified window.
     * @param itemTitle Name that the menu item is called.
     * @param windowTitle Title that appears at the top of the window.
     * @param fxmlPath Path to the .fxml file for the window.
     * @param width The starting width for the window.
     * @param height The starting height for the window.
     */
    public MenuItemToWindow(String itemTitle, String windowTitle, String fxmlPath, double width, double height, boolean resizabe) {
        super(itemTitle);

        addEventHandler(EventType.ROOT, event -> {
            stage = new Stage();
            stage.setTitle(windowTitle);
            stage.setResizable(resizabe);

            // load .fxml file
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(fxmlPath));

            Scene scene = null;
            try {
                scene = new Scene(fxmlLoader.load(), width, height);
            } catch (IOException e) {
                Main.log(Level.WARNING, "Failed to load window from %s file.", fxmlPath);
            }

            menu = fxmlLoader.getController();
            menu.onOpen();

            stage.setScene(scene);
            stage.show();
        });
    }

    /**
     * Simply set the information in a menu item to open a specified window.
     * @param itemTitle Name that the menu item is called.
     * @param windowTitle Title that appears at the top of the window.
     * @param fxmlPath Path to the .fxml file for the window.
     */
    public MenuItemToWindow(String itemTitle, String windowTitle, String fxmlPath) {
        this(itemTitle, windowTitle, fxmlPath, 320, 240, false);
    }

    /**
     * Get stage object for this window.
     * @return Stage object.
     */
    protected Stage getStage() {
        return stage;
    }

    /**
     * Get the Window's controller class.
     * @return WindowController class which can be cast to a child if applicable.
     */
    protected WindowController getController() {
        return menu;
    }
}
