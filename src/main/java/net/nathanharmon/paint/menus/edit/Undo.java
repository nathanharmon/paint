/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.edit;

import javafx.event.EventType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.nathanharmon.paint.drawing.TabManager;

public class Undo extends MenuItem {

    public Undo(TabManager tabs) {
        super("Undo");
        setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN));
        addEventHandler(EventType.ROOT, event -> tabs.getCurrentlySelectedTab().undo());
    }
}
