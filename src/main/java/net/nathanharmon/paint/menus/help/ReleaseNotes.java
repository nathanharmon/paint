/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.help;

import net.nathanharmon.paint.menus.MenuItemToWindow;

/**
 * Help -> Release Notes | Display the release notes for the current version.
 */
public class ReleaseNotes extends MenuItemToWindow {

    public ReleaseNotes() {
        super("View Release Notes", "Pain(t) Release Notes", "release-notes-window.fxml", 640, 850, false);
    }
}
