/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.file;

import javafx.event.EventType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.FileUtilities;

import java.io.File;

/**
 * File -> Save | Save file
 */
public class SaveFile extends MenuItem {

    public SaveFile(TabManager tabs) {
        super("Save");
        setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        addEventHandler(EventType.ROOT, event -> {
            // don't do anything if there is not a tab currently opened.
            if(tabs.getCurrentlySelectedTab() != null) {
                File file;
                if (!tabs.getCurrentlySelectedTab().getActions().isSaved())
                    // prompt user for saving location
                    file = FileUtilities.saveImage("Save", tabs.getCurrentlySelectedTab().getText());
                else
                    file = tabs.getCurrentlySelectedTab().getActions().getSaveFile();

                // store file object
                tabs.getCurrentlySelectedTab().getActions().setSaveFile(file);

                // mark this tab as saved
                tabs.getCurrentlySelectedTab().getActions().setSaved(true);

                assert file != null; // if the tab is saved, then a save file exists and is not null
                FileUtilities.writeImageToFile(tabs.getCurrentlySelectedTab().getActions().getImage(), file);

                // set tab title name
                tabs.getCurrentlySelectedTab().setText(file.getName());
            }
        });
    }
}
