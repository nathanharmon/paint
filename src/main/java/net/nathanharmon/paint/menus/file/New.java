/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.file;

import javafx.event.EventType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.nathanharmon.paint.drawing.CanvasTab;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.menus.MenuItemToWindow;
import net.nathanharmon.paint.windows.NewWindow;

/**
 * File -> New | Create a blank canvas to write on.
 */
public class New extends MenuItemToWindow {

    public New(TabManager tabs) {
        super("New", "Select Canvas Size", "new-window.fxml", 400, 300, false);
        setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));

        addEventHandler(EventType.ROOT, e -> getStage().setOnCloseRequest(event -> {
            NewWindow window = (NewWindow) getController();

            double width = window.getX();
            double height = window.getY();

            CanvasTab tab = tabs.createTab(null, true);
            tab.getActions().createCanvas(width, height);
        }));
    }
}
