/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.file;

import javafx.event.EventType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.nathanharmon.paint.Main;
import net.nathanharmon.paint.drawing.CanvasTab;
import net.nathanharmon.paint.drawing.TabManager;
import net.nathanharmon.paint.helper.file.FileUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;

/**
 * File -> Open | Open a file chooser dialog and open the picture on the canvas.
 */
public class OpenFile extends MenuItem {

    public OpenFile(TabManager tabs) {
        super("Open");
        setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        addEventHandler(EventType.ROOT, event -> {
            File file = FileUtilities.getImageFile("Choose a picture");

            if(file != null) {
                CanvasTab tab = tabs.createTab(file.getName(), true);

                tabs.setCurrentlySelectedTab(tab);
                try {
                    tabs.getCurrentlySelectedTab().getActions().drawInitialImage(new FileInputStream(file));
                } catch (FileNotFoundException e) {
                    Main.log(Level.SEVERE, "The user selected a file that doesn't exist.");
                }
                tabs.getCurrentlySelectedTab().getActions().setSaveFile(file);
            }
        });
    }

}
