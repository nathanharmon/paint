/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint.menus.file;

import javafx.event.EventType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.nathanharmon.paint.drawing.TabManager;

/**
 * File -> Exit | Close the program gracefully.
 */
public class Exit extends MenuItem {

    public Exit(TabManager tabs) {
        super("Exit");
        setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
        addEventHandler(EventType.ROOT, event -> tabs.safeExit());
    }
}
