/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.nathanharmon.paint.helper.file.FileUtilities;
import net.nathanharmon.paint.helper.file.Language;
import net.nathanharmon.paint.windows.MainWindow;

import java.io.IOException;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Pain(t). The Paintful yet clean paint program.
 * @author Nathan Harmon
 */
public class Main extends Application {

    public static final String VERSION = "1.0.0";

    private static Logger log;

    /**
     * A helper method to perform logging for the application.
     * @param level The category of log. This project uses the options:
     * CONFIG: Any routine logging that deals with settings or saving and loading files.
     * INFO: Any routine logging that completed successfully.
     * WARNING: When a task completes but not entirely successfully.
     * SEVERE: When a task fails.
     * @param message Message to log.
     * @param args Any printf arguments to apply to "msg"
     */
    public static void log(Level level, String message, Object... args) {
        log.log(level, String.format(message, args));
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("main-window.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 640, 480);

        MainWindow menu = fxmlLoader.getController();
        menu.onOpen(stage);

        stage.setTitle("Pain(t)");
        stage.setMaximized(true);

        // display the main window
        stage.setMinWidth(640);
        stage.setMinHeight(480);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        setupLogging();

        // Log to file
        try {
            log.addHandler(new FileHandler(FileUtilities.getCacheFolder() + "paintlog-" + new Date().toString() + ".log"));
        } catch (IOException e) {
            log(Level.SEVERE, "Failed to load log file.");
        }

        Language.load("en");

        /*
         * Launch Initial Window
         */
        launch();
    }

    /**
     * Load logging utilities for use in the program.
     */
    public static void setupLogging() {
        /*
         * Setup LogManager
         */
        log = LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME);
        log.setLevel(Level.ALL);
    }

    /**
     * Tests if logging has been set up yet.
     * @return True if logging is not yet set up.
     * @see #setupLogging()
     */
    public static boolean isNotLogging() {
        return log == null;
    }
}