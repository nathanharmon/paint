/*
 * Copyright (c) 2021 Nathan Harmon
 */

package net.nathanharmon.paint;

/**
 * A main class to be run by the IDE as a helper runner class.
 * The standard main class is called "Main".
 * @see net.nathanharmon.paint.Main
 */
public class IdeMain {

    public static void main(String[] args) {
        Main.main(args);
    }
}
