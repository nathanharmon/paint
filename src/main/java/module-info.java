module net.nathanharmon.paint {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.logging;
    requires org.jetbrains.annotations;
    requires java.desktop;
    requires javafx.swing;
    requires org.apache.commons.io;
    requires semver;
    requires org.yaml.snakeyaml;
    requires org.junit.jupiter.api;


    opens net.nathanharmon.paint to javafx.fxml;
    exports net.nathanharmon.paint;
    exports net.nathanharmon.paint.canvas;
    exports net.nathanharmon.paint.windows;
    opens net.nathanharmon.paint.windows to javafx.fxml;
    exports net.nathanharmon.paint.helper.file;
    opens net.nathanharmon.paint.helper.file to javafx.fxml;
    exports net.nathanharmon.paint.drawing;
    opens net.nathanharmon.paint.drawing to javafx.fxml;
}